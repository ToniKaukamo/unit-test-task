const expect = require("chai").expect;
const ArithmeticOperations = require("../mylib");

describe("Testing mylib functions", () => {
    before(()=> {
        console.log("Test starting.");
    });
    it("Constructor converts string 'numbers' to float properly", () => {
        // If this works there will not be concatenation "errors"
        const num1 = "7";
        const num2 = "6";
        const convertTest = new ArithmeticOperations(num1,num2).addition();
        expect(typeof(convertTest)).to.equal("number");
    });
    it("Constructor won't accept non number characters", () => {
        const num1 = "@";
        const num2 = "a";
        expect(() => {new ArithmeticOperations(num1,num2)})
            .to.throw();
    });
    it("Can do addition", () => {
        const num1 = 7;
        const num2 = 6;
        addition = new ArithmeticOperations(num1,num2).addition();
        expect(addition).to.equal(num1+num2)
    });
    it("Can do subtraction", () => {
        const num1 = 2;
        const num2 = 6;
        subtraction = new ArithmeticOperations(num1,num2).subtraction();
        expect(subtraction).to.equal(num1-num2);
    });
    it("Can do multiplication", () => {
        const num1 = 2;
        const num2 = 6;
        multiplication = new ArithmeticOperations(num1,num2).multiplication();
        expect(multiplication).to.equal(num1*num2);
    });
    it("Can catch ZeroDivision error", () => {
        const num1 = 2;
        const num2 = 0;
        expect(() => {new ArithmeticOperations(num1,num2).division()})
            .to.throw();
    });
    it("Can do division", () => {
        const num1 = 2;
        const num2 = 6;
        division = new ArithmeticOperations(num1,num2).division();
        expect(division).to.equal(num1/num2);
    });
    after(()=>{
        console.log("Test ending.")
    });
});