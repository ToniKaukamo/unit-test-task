class ArithmeticOperations {
    /**@typedef (number) */
    num1
    /**@typedef (number) */
    num2
    constructor(num1 = 1, num2 = 1) {
        if (isNaN(parseFloat(num1))) {
            throw new Error("num1 is not a number");
        }
        if (isNaN(parseFloat(num2))) {
            throw new Error("num2 is not a number");
        }
        this.num1 = parseFloat(num1);
        this.num2 = parseFloat(num2);
    }
    addition() {
    	return this.num1 + this.num2;
    }
  
    subtraction() {
    	return this.num1 - this.num2;
    }
  
    multiplication() {
    	return this.num1 * this.num2;
    }
  
    division() {
		if (this.num2 === 0) {
			throw new Error("ZeroDivision");
		}
		return this.num1 / this.num2;
	}
};

module.exports = ArithmeticOperations;