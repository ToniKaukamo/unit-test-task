# Basic arithmetic operations - Unit test task
This repository is used for a task. It has a library that contains some basic aritmetic operations.

My version of the lib is abit different from compared to the lecture slides.
#### Program structure
```
C:\Users\Toni\projects\devops-s23\unit_test
├── .gitignore
├── commands.sh
├── main.js
├── mylib.js
├── package-lock.json
├── package.json
├── README.md
└── test
   └── mylib.spec.js
```