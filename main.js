const ArithmeticOperations = require("./mylib");

console.log("Calculator starting");

calculator = new ArithmeticOperations(15,3);
console.log(`Addition: ${calculator.num1} + ${calculator.num2} = ${calculator.addition()}`);
console.log(`Subtraction: ${calculator.num1} - ${calculator.num2} = ${calculator.subtraction()}`);
console.log(`Multiplication: ${calculator.num1} * ${calculator.num2} = ${calculator.multiplication()}`);
console.log(`Division: ${calculator.num1} / ${calculator.num2} = ${calculator.division()}`);

console.log("Calculating done quitting.");